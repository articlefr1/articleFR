<?
	/*************************************************************************************************************************
	*
    * Free Reprintables Article Directory System
    * Copyright (C) 2014  Glenn Prialde

    * This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
	* 
	* Author: Glenn Prialde
	* Contact: admin@freecontentarticles.com
	* Mobile: +639473473247	
	*
	* Website: http://freereprintables.com 
	* Website: http://www.freecontentarticles.com 
	*
	*  PLEASE GO TO HTTP://WWW.ISNARE.COM/PUBLISHER AND REGISTER AN ACCOUNT AND SET YOUR SETTINGS 
	*  TO ACTIVATE THIS FEATURE.
	*
	*************************************************************************************************************************/
	
	define ( 'ROOT_DIR', realpath ( dirname ( __FILE__ ) ) . '/' );
	define ( 'SYS_DIR', ROOT_DIR . 'system/' );
	define ( 'APP_DIR', ROOT_DIR . 'application/' );
	
	include_once( dirname(__FILE__) . '/application/config/config.php' );	
	require_once( dirname(__FILE__) . '/system/mysqli.functions.php' );
	include_once( dirname(__FILE__) . '/system/functions.php' );	
	
	$_ini = parse_ini_file(dirname(__FILE__) . '/application/config/isnare.ini', true);
	
	//if ($_SERVER['REMOTE_ADDR'] == gethostbyname("apollo.isnare.com")) {
		$title = $_REQUEST["title"];
		$author = $_REQUEST["author"];
		$summary = NULL;
		$category = $_REQUEST["category"];
		$body_text = NULL;
		$body_html = $_REQUEST["content"];
		$bio_text = NULL;
		$bio_html = $_REQUEST["about"];
		$keywords = $_REQUEST["keywords"];
		$email = $_REQUEST["email"]; 	
		
		if (!isset($email)) {
			$email = "admin@freecontentarticles.com";
		}
		
		$_conn = new_db_conni($config['db_host'], $config['db_username'], $config['db_password'], $config['db_name']);
		$_retval = submitArticle('admin', $title, $category, $_ini['meta']['author'], $summary, $body_html, $bio_html, $_conn);
		print_r($_retval);
		close_db_conni($_conn);
	//}
?>
